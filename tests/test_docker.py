import pytest
import requests
import base64
import json
from tensorflow.keras.datasets.mnist import load_data
import numpy as np
#load MNIST dataset
(_, _), (x_test, y_test) = load_data()
# reshape data to have a single channel
x_test = x_test.reshape((x_test.shape[0], x_test.shape[1], x_test.shape[2],1))
# normalize pixel values
x_test = x_test.astype('float32') / 255.0
#server URL
# url = 'http://digit_server:8501/v1/models/img_classifier:predict' #see [B]
url = 'https://dlmodelapp-9e3n.onrender.com/v1/models/img_classifier:predict' 
def make_prediction(instances):
    data = json.dumps({"signature_name": "serving_default","instances": instances.tolist()}) #see [C]
    headers = {"content-type": "application/json"}
    json_response = requests.post(url, data=data, headers=headers)
    predictions = json.loads(json_response.text)['predictions']
    return predictions

def test_prediction():
    predictions = make_prediction(x_test[0:4]) #see [A]
    for i, pred in enumerate(predictions):
        assert y_test[i] == np.argmax(pred) #see [D]